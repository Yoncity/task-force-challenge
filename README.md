# Task Force Challenge

This project is bootsrapped with create-react-up with Typscript template.

### What does it do?

This web-app returns the previous and current day's covid cases of a selected country.

## How to run this app.

- First you will need to clone the node app.
- Then you can use **yarn** or **npm** to install the necessary packages.
- Then you can use **yarn start** or **npm run start** to run the application.

### Comments

The backend you provided and the template didn't have the same type of data and so i removed some of the elements specified in the design.
